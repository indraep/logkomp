public class MiniSat {
	int totalClause = 0;
	
	MiniSat() {
		int n = 4;
		String cnf = nQueen(n);
		
		cnf = "p cnf " + (n * n) + " " + totalClause + "\n" + cnf;
		
		System.out.println(cnf);
	}
	
	private int getIndex(int r, int c, int n) {
		return (r - 1) * n + c;
	}
	
	private String nQueen(int n) {
		String ret = "";
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				ret += getHorizontalRule(i, j, n);
				ret += getVerticalRule(i, j, n);
				ret += getDiagonalRule(i, j, n);
			}
			ret += getOneQueenInEachRowRule(i, n);
		}
		return ret;
	}
	
	private String getOneQueenInEachRowRule(int r, int n) {
		String ret = "";
		
		int id;
		for (int i = 1; i <= n; i++) {
			id = getIndex(r, i, n);
			ret += id + " ";
		}
		totalClause++;
		return ret + "0\n";
	}
	
	private String getHorizontalRule(int r, int c, int n) {
		String ret = "";
		
		int curId = getIndex(r, c, n), id;
		for (int i = 1; i <= n; i++) {
			if (i == c)
				continue;
			
			id = getIndex(r, i, n);
			ret += "-" + curId + " " + "-" + id + " 0\n";
			totalClause++;
		}
		
		return ret;
	}
	
	private String getVerticalRule(int r, int c, int n) {
		String ret = "";
		
		int curId = getIndex(r, c, n), id;
		for (int i = 1; i <= n; i++) {
			if (i == r)
				continue;
			
			id = getIndex(i, c, n);
			ret += "-" + curId + " " + "-" + id + " 0\n";
			totalClause++;
		}
		return ret;
	}
	
	private String getDiagonalRule(int r, int c, int n) {
		String ret = "";
		
		int curId = getIndex(r, c, n), id;
		int rr = r - Math.min(r, c) + 1;
		int cc = c - Math.min(r, c) + 1;
		while (rr <= n && cc <= n) {
			if (rr == r && cc == c) {
				rr++; cc++;
				continue;
			}
			
			id = getIndex(rr++, cc++, n);
			ret += "-" + curId + " " + "-" + id + " 0\n";
			totalClause++;
		}
		
		
		rr = r + Math.min(n - r, c - 1);
		cc = c - Math.min(n - r, c - 1);
		while (cc <= n && rr >= 1) {
			if (rr == r && cc == c) {
				rr--; cc++;
				continue;
			}
			
			id = getIndex(rr--, cc++, n);
			ret += "-" + curId + " " + "-" + id + " 0\n";
			totalClause++;
		}
		
		return ret;
	}
	
	public static void main (String [] ar) {
		new MiniSat();
	}
}
